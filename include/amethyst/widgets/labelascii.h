#include <mt_gemx.h>
#include <string>

#include "amethyst/label.h"
#include "amethyst/widget.h"

class LabelASCII : public Label
{
    public:
    char text[200];

    LabelASCII(GEMWindow *window,
            GEMWidget *parent,
            std::string name,
            short x, short y,
            short font_size) : Label(window, parent, name, x, y, font_size) { }

    void Draw(short vdi_handle);
};
