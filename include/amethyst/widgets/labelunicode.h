/***
 * LabelUnicode
 * 
 * A widget that provides a Unicode (UTF-16) label.
 * 
 * window: the window this widget is a child of
 * name: the name of this widget
 * x, y: coordinates on window
 * font_size: the text size, in points
 ***/

#pragma once

#include <mt_gemx.h>
#include <string>

#include "amethyst/widget.h"
#include "amethyst/label.h"

class LabelUnicode : public Label
{
    private:
    size_t text_length = 200;

    public:
    unistring_uint16_t text[200];

    LabelUnicode(GEMWindow *window,
            GEMWidget *parent,
            std::string name,
            short x, short y,
            short font_size) : Label(window, parent, name, x, y, font_size) { }

    void Draw(short vdi_handle);

    /* SetTextFromUTF8(unistring_uint8_t * utf8_str)
     *
     * Most Unicode information that comes in from the internet is in UTF-8 format.
     * NVDI expects UTF-16 data, though, so this function converts a UTF-8 string to
     * its UTF-16 equivalent and stores it in the label's text field.
     */ 
    void SetTextFromUTF8(unistring_uint8_t *utf8_str);
};
