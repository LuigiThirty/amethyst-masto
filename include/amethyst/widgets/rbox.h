/***
 * RBox
 * 
 * Widget wrapper for vdi_rbox
 * 
 * window: the window this widget is a child of
 * name: the name of this widget
 * x, y: coordinates on window
 * w, h: width and height
 ***/

#pragma once

#include "amethyst/widget.h"

class RBox : public GEMWidget
{
    public:
    short w, h;

    RBox(
        GEMWindow *window,
        GEMWidget *parent,
        std::string name,
        short x,
        short y,
        short w,
        short h) : GEMWidget(window, parent, name, x, y) 
        {
            this->w = w;
            this->h = h;
        }

    void Draw(short vdi_handle);
};
