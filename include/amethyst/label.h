#pragma once

#include "amethyst/widget.h"

class Label : public GEMWidget
{
    public:
    int font_size;

    Label(GEMWindow *window,
            GEMWidget *parent,
            std::string name,
            short x,
            short y,
            short font_size) : GEMWidget(window, parent, name, x, y) 
            {
                this->font_size = font_size;
            }
};
