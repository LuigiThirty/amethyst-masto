#pragma once

#include <gem.h>
#include <mt_gem.h>
#include <cstdlib>
#include <string>
#include <list>

#include <functional>
#include <algorithm>
#include <string>
#include <list>

#include "globals.h"
#include "rectangle.h"

class GEMWidget;

typedef struct {
    short x;
    short y;
} GEMPoint;

class GEMWindow
{
    protected:
    std::list <GEMWidget *> widgets; 

    public:
    GEMPoint Position;
    short w;
    short h;

    virtual void Show() = 0;

    GEMWidget *FindWidgetByName(std::string name);

    void RedrawBackground(GRECT *dirty);
};
