#pragma once

#include <unistr.h>
#include <string>

#include "amethyst/window.h"

class GEMWidget {
    private:
    int id;
    
    protected:
    GEMWindow *window;
    GEMWidget *parent;  // can be NULL

    std::string name;
    static int current_id;

    short x;
    short y;

    public:
    GEMWidget(GEMWindow *window, GEMWidget *parent, std::string name, short x, short y)
    {
        this->id = current_id++;
        this->window = window;
        this->parent = parent;
        this->name = name;
        this->x = x;
        this->y = y;
    }

    std::string GetName() { return name; }

    virtual void Draw(short vdi_handle) = 0;
};