#ifndef GLOBALS_H__
#define GLOBALS_H__

#include <cstring>
#include <mt_gem.h>
#include <stdint.h>

#define COLOR_WHITE 0
#define COLOR_BLACK 1

#define PTR_HIWORD(X) (uint16_t)(((uint32_t)(X) & 0xFFFF0000) >> 16)
#define PTR_LOWORD(X) (uint16_t)(((uint32_t)(X) & 0x0000FFFF))

extern short hwchar, hhchar, hwbox, hhbox;
extern short appl_id, aes_handle, vdi_handle;
extern short screen_width, screen_height;
extern "C" { extern short aes_global[AES_GLOBMAX]; }
extern short desktop_usable_x, desktop_usable_y, desktop_usable_w, desktop_usable_h;
extern short nil;

extern GRECT screen_full;

extern VDIPB vdi_param_blk;

extern char str_temp_buf[1000];
extern size_t str_temp_len;

extern short pixelxy[4];

typedef struct text_attributes_t
{
  short font_id;
  short text_color;
  short text_rotation;
  short horiz_alignment;
  short vert_alignment;
  short writing_mode;
  short char_width;
  short char_height;
  short cell_width;
  short cell_height;
} text_attributes;

typedef union text_attributes_u {
  text_attributes s;
  short arr[10];
} text_attributes_union;


#endif
