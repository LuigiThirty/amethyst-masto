#pragma once

#include <windom.h>
#include <cstdio>
#include <cstring>
#include <list>
#include <unistr.h>

#include "jsmn/jsmn.h"
#include "amethyst/window.h"
#include "amethyst/widget.h"
#include "amethyst/widgets/rbox.h"
#include "amethyst/widgets/labelunicode.h"
#include "amethyst/widgets/labelascii.h"
#include "mastodon/account.h"

class AccountWindow : public GEMWindow
{
    MastodonAccount currentAccount;
    short handle;

    public:
    AccountWindow();

    void Show();
    void Redraw(GRECT *dirty);

    short GetHandle() { return handle; }
};
