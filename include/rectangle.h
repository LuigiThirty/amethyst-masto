#pragma once

#include <mt_gem.h>

void make_grect(GRECT *rectangle, short x, short y, short w, short h);

class Rectangle
{
    short x, y, w, h;

    Rectangle()
    {
        this->x = 0;
        this->y = 0;
        this->w = 0;
        this->h = 0;
    }

    Rectangle(short x, short y, short w, short h)
    {
        this->x = x;
        this->y = y;
        this->w = w;
        this->h = h;
    }

    public:
    void Draw(int vdi_handle, GRECT *window);
};
