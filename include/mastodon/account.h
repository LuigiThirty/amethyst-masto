#pragma once

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cstdbool>

#include <unistdio.h>
#include <unistr.h>
#include <stdio.h>
#include <cwchar>

#include "../jsmn/jsmn.h"

static int jsoneq(const char *json, jsmntok_t *tok, const char *s) {
	if (tok->type == JSMN_STRING && (int) strlen(s) == tok->end - tok->start &&
			strncmp(json + tok->start, s, tok->end - tok->start) == 0) {
		return 0;
	}
	return -1;
}

class MastodonAccount
{
    jsmntok_t tokens[64];

    int id;
    char username[32];
    char acct[32];
    char display_name[32];
    bool locked;
    char created_at[32];
    unistring_uint8_t note[500]; // UTF-8
    char url[100];
    char avatar[200];
    char header[200];
    char header_static[200];
    int followers_count;
    int following_count;
    int statuses_count;

    unsigned short utf16_buffer[512];
    size_t utf16_buffer_size = 512;

    public:
    char *GetUsername() { return username; }
    char *GetDisplayName() { return display_name; }
    int GetFollowersCount() { return followers_count; }
    int GetFollowingCount() { return following_count; }
    int GetStatusesCount() { return statuses_count; }
    unistring_uint8_t *GetNote() { return note; }
    unistring_uint16_t *GetNoteAsUTF16()
    { 
        u8_to_u16(note, u8_strlen(note), utf16_buffer, &utf16_buffer_size);
        return utf16_buffer;
    }

    MastodonAccount() {}
    MastodonAccount(char *json);
};