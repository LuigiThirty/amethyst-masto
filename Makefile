CC=m68k-atari-mint-gcc
INCLUDE=include

LIBS = -lwindom -lgem -lldg -lunistring -lstdc++

.PHONY: clean upload all

SRCFILES = 	src/mastodon.cpp \
		src/account_window.cpp \
		src/rectangle.cpp \
		src/globals.cpp \
		src/vdi_helpers.cpp \
		src/jsmn/jsmn.c \
		src/mastodon/account.cpp \
		src/amethyst/widgets/rbox.cpp \
		src/amethyst/widgets/labelunicode.cpp \
		src/amethyst/widgets/labelascii.cpp \
		src/amethyst/widget.cpp \
		src/amethyst/window.cpp

mastodon.prg: $(SRCFILES)
	$(CC) -I$(INCLUDE) $^ $(LIBS) -o $@

all: mastodon.prg upload

clean:
	rm *.prg

upload:
	sh upload.sh
