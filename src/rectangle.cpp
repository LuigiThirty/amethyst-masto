#include "rectangle.h"

void make_grect(GRECT *rectangle, short x, short y, short w, short h)
{
  /* Populate a GRECT with the values given. */
  rectangle->g_x = x;
  rectangle->g_y = y;
  rectangle->g_w = w;
  rectangle->g_h = h;
}

void Rectangle::Draw(int vdi_handle, GRECT *window)
{
    GRECT rect;
    short rect_arr[4];

    make_grect(&rect, window->g_x+x, window->g_y+y, w, h);
    grect_to_array(&rect, rect_arr);
    v_bar(vdi_handle, rect_arr);
}