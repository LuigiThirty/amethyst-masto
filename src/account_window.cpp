#include "account_window.h"

char account_response_test[] = "{\"id\":\"64621\",\"username\":\"luigithirty\",\"acct\":\"luigithirty\",\"display_name\":\"Dune, Dune, Dune of Earl\",\"locked\":false,\"bot\":false,\"created_at\":\"2018-03-16T13:34:01.584Z\",\"note\":\"\u003cp\u003e⚧🚺 68k witch - emulator writer - world’s greatest Atari Jaguar programmer 🚺⚧\u003c/p\u003e\",\"url\":\"https://cybre.space/@luigithirty\",\"avatar\":\"https://static.cybre.space/accounts/avatars/000/064/621/original/691b330baf4ab5b3.jpeg\",\"avatar_static\":\"https://static.cybre.space/accounts/avatars/000/064/621/original/691b330baf4ab5b3.jpeg\",\"header\":\"https://cybre.space/headers/original/missing.png\",\"header_static\":\"https://cybre.space/headers/original/missing.png\",\"followers_count\":322,\"following_count\":302,\"statuses_count\":2089,\"emojis\":[],\"fields\":[{\"name\":\"Gender\",\"value\":\"she/her\",\"verified_at\":null},{\"name\":\"Location\",\"value\":\"Florida!\",\"verified_at\":null}]}";

AccountWindow::AccountWindow()
{
    printf("Instantiating account window\n");
    char title[] = "Account";

    /* Get desktop info */
    wind_get(0, WF_WORKXYWH, &desktop_usable_x, &desktop_usable_y,
        &desktop_usable_w, &desktop_usable_h);

    this->handle = wind_create(NAME|CLOSER,
                    desktop_usable_x, desktop_usable_y,
                    desktop_usable_w, desktop_usable_h);

    mt_wind_set(handle, WF_NAME, PTR_HIWORD(&title), PTR_LOWORD(&title), 0, 0, aes_global);

    widgets.push_back(new LabelUnicode(
        this, 
        NULL,
        "username",
        10, 40,
        12));
    widgets.push_back(new LabelUnicode(
        this,
        NULL,
        "note",
        10, 110,
        12));

    widgets.push_back(new RBox(
        this, 
        NULL,
        "followerbox", 
        60, 50, 
        280, 40));
    widgets.push_back(new LabelASCII(
        this,
        NULL,
        "followers",
        70, 68,
        10));
    widgets.push_back(new LabelASCII(
        this, 
        NULL,
        "following", 
        220, 68, 
        10));
    widgets.push_back(new LabelASCII(
        this, 
        NULL,
        "statuses", 
        70, 84, 
        10));

}

void AccountWindow::Show()
{
    printf("Showing account window\n");

    wind_open(handle, 
        desktop_usable_x, desktop_usable_y,
        400, 300);

    currentAccount = MastodonAccount(account_response_test);

    LabelUnicode *usernamelbl = (LabelUnicode *)FindWidgetByName("username");
    sprintf(str_temp_buf, "%s (@%s)", currentAccount.GetDisplayName(), currentAccount.GetUsername());
    usernamelbl->SetTextFromUTF8((unistring_uint8_t *)str_temp_buf);

    LabelUnicode *notelbl = (LabelUnicode *)FindWidgetByName("note");
    notelbl->SetTextFromUTF8(currentAccount.GetNote());

    LabelASCII *followerslbl = (LabelASCII *)FindWidgetByName("followers");
    sprintf(followerslbl->text, "Followers: %d", currentAccount.GetFollowersCount());
    LabelASCII *followinglbl = (LabelASCII *)FindWidgetByName("following");
    sprintf(followinglbl->text, "Following: %d", currentAccount.GetFollowingCount());
    LabelASCII *statuseslbl = (LabelASCII *)FindWidgetByName("statuses");
    sprintf(statuseslbl->text, "Toots: %d", currentAccount.GetStatusesCount());
}

void AccountWindow::Redraw(GRECT *dirty)
{
    printf("Redrawing account window\n");

    // Fetch the window's location and size, update our local copy.
    wind_get(handle, WF_CURRXYWH, &(Position.x), &(Position.y), &w, &h);
    
    // Redraw any dirty background rectangles.
    RedrawBackground(dirty);

    ((RBox *)FindWidgetByName("followerbox"))->Draw(vdi_handle);

    // Redraw our widgets.
    LabelUnicode *usernamelbl = (LabelUnicode *)FindWidgetByName("username");
    sprintf(str_temp_buf, "%s (@%s)", currentAccount.GetDisplayName(), currentAccount.GetUsername()); // from Mastodon response
    usernamelbl->SetTextFromUTF8((unistring_uint8_t *)str_temp_buf); // Mastodon JSON response is UTF-8
    usernamelbl->Draw(vdi_handle); // draws the text to the screen with VDI

    ((LabelASCII *)FindWidgetByName("followers"))->Draw(vdi_handle);
    ((LabelASCII *)FindWidgetByName("following"))->Draw(vdi_handle);
    ((LabelASCII *)FindWidgetByName("statuses"))->Draw(vdi_handle);

    ((LabelUnicode *)FindWidgetByName("note"))->Draw(vdi_handle);
}