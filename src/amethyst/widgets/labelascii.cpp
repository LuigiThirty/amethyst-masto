#include "amethyst/widgets/labelascii.h"

void LabelASCII::Draw(short vdi_handle)
{
    vst_point(vdi_handle, font_size, &nil, &nil, &nil, &nil);
    v_ftext(vdi_handle, window->Position.x+x, window->Position.y+y, text);
}