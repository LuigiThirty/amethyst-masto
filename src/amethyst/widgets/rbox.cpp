#include "amethyst/widgets/rbox.h"

void RBox::Draw(short vdi_handle)
{
    pixelxy[0] = window->Position.x+x;
    pixelxy[1] = window->Position.y+y;
    pixelxy[2] = window->Position.x+x+w;
    pixelxy[3] = window->Position.y+y+h;
    v_rbox(vdi_handle, pixelxy);
}