#include "amethyst/widgets/labelunicode.h"

void LabelUnicode::Draw(short vdi_handle)
{
    vst_point(vdi_handle, font_size, &nil, &nil, &nil, &nil);
    v_ftext16(vdi_handle, window->Position.x+x, window->Position.y+y, (WCHAR *)text);
}

void LabelUnicode::SetTextFromUTF8(unistring_uint8_t *utf8_str)
{
    unistring_uint16_t *loc;
    loc = u8_to_u16(utf8_str, 100, text, &text_length);

    if(loc != text)
    {
        printf("*** error: SetTextFromUTF8 in widget '%s' failed\n", name);
    }
}