#include "amethyst/window.h"
#include "amethyst/widget.h"

bool WidgetHasName(GEMWidget *widget, std::string name)
{
    return (widget->GetName() == name);
}

GEMWidget * GEMWindow::FindWidgetByName(std::string name)
{
    std::list<GEMWidget *>::iterator it;
    it = std::find_if(widgets.begin(), widgets.end(), [&] (GEMWidget *w) { return w->GetName() == name; });

    return *it;
}

void GEMWindow::RedrawBackground(GRECT *dirty)
{
    GRECT window;
    short pixelxy[4];
    bool temp_dirty = false;

    make_grect(&window, Position.x, Position.y, w, h);

    if(dirty == NULL)
    {
        dirty = (GRECT *)malloc(sizeof(GRECT));
        dirty->g_x = Position.x;
        dirty->g_y = Position.y;
        dirty->g_w = w;
        dirty->g_h = h;
        temp_dirty = true;
    }

    /* Fill the clip area with the white background. */
    grect_to_array(dirty, pixelxy);
    vsf_color(vdi_handle, COLOR_WHITE);
    v_bar(vdi_handle, pixelxy);
}