#include "mastodon.h"

char recv_buffer[65536];

#define MAXBUF 65536

AccountWindow accountWindow;

char * getMastodonAccountInfo()
{
	int sockfd, bytes_read;
    struct sockaddr_in dest;
    char buffer[MAXBUF];

	sockfd = socket(AF_INET, SOCK_STREAM, 0);

	struct timeval tv;
	tv.tv_sec = 1;
	tv.tv_usec = 0;
	setsockopt(sockfd, SOL_SOCKET, SO_RCVTIMEO, (const char*)&tv, sizeof tv);

    /*---Initialize server address/port struct---*/
    memset(&dest, 0, sizeof(dest));
    dest.sin_family = AF_INET;
    dest.sin_port = htons(8888); /*default HTTP Server port */
    dest.sin_addr.s_addr = inet_addr("10.0.0.236");

	/*---Connect to server---*/
    connect(sockfd, (struct sockaddr*)&dest, sizeof(dest));

	char request[] = "GET /api/v1/accounts/64621 HTTP/1.1\r\nHost: 127.0.0.1:8888\r\nUser-Agent: AtariTT/1.0\r\n\r\n";
	send(sockfd, request, strlen(request), 0);

	printf("Request sent, reading response:\n");

	char *response = (char *)malloc(65536);
	int total_bytes_read = 0;

	/*---While there's data, read and print it---*/
    do
    {
        memset(buffer, 0, MAXBUF);
        bytes_read = recv(sockfd, buffer, MAXBUF, 0);
        if ( bytes_read > 0 )
		{
            //printf("%s", buffer);
			printf("Read a chunk of %d bytes\n", bytes_read);
			sprintf(response+total_bytes_read, "%s", buffer);
			total_bytes_read += bytes_read;
		}
    }
    while ( bytes_read > 0 );

	printf("*** Done\n");
	close(sockfd);

	printf(response);

	// Find the JSON string in the response.
	// Should start with {"id":
	char *json_start = strstr(response, "{\"id\":");
	if(json_start)
	{
		// We found it.
		char *json = (char *)malloc(MAXBUF);
		memcpy(json, json_start, strlen(json_start));
		return json;
	}

	return NULL;
}

bool gem_init()
{
	printf("*** Initializing GEM for Mastodon\n");

	appl_id = mt_appl_init(aes_global);
	aes_handle = graf_handle(&hwchar, &hhchar, &hwbox, &hhbox);

	vdi_param_blk.control 	= (short *)malloc(VDI_CNTRLMAX);
	vdi_param_blk.intin 	= (short *)malloc(VDI_INTINMAX);
	vdi_param_blk.intout 	= (short *)malloc(VDI_INTOUTMAX);
	vdi_param_blk.ptsin 	= (short *)malloc(VDI_PTSINMAX);
	vdi_param_blk.ptsout 	= (short *)malloc(VDI_PTSOUTMAX);

	/* set up intin so we can open a screen */
	vdi_param_blk.intin[0] = Getrez() + 2; /* device driver */
	vdi_param_blk.intin[1] = 1; /* line type = solid */
	vdi_param_blk.intin[2] = 1; /* line color = black */
	vdi_param_blk.intin[3] = 1; /* marker type = dot */
	vdi_param_blk.intin[4] = 1; /* marker color = black */
	vdi_param_blk.intin[5] = 1; /* system font */
	vdi_param_blk.intin[6] = 1; /* font color = black */
	vdi_param_blk.intin[7] = 1; /* fill style = solid */
	vdi_param_blk.intin[8] = 1; /* fill index = no effect */
	vdi_param_blk.intin[9] = 1; /* fill color = black */
	vdi_param_blk.intin[10]= 2; /* raster coordinates */

	v_opnvwk(vdi_param_blk.intin, &vdi_handle, vdi_param_blk.intout);

	/* Grab the current desktop resolution. */
	screen_width = vdi_param_blk.intout[0];
	screen_height = vdi_param_blk.intout[1];
	screen_full.g_x = 0;
	screen_full.g_y = 0;
	screen_full.g_w = screen_width+1;
	screen_full.g_h = screen_height+1;

	// Load the font list and look for FreeSerif.
	int num_fonts = vst_load_fonts(vdi_handle, 0);
	printf("VDI loaded %d fonts\n", num_fonts);

	char font_name[32];
	for(int i=0; i<num_fonts; i++)
	{
		vqt_name(vdi_handle, 2+i, font_name); // User fonts start at index 2
		printf("Font %d is '%s'\n", 2+i, font_name);
	}

	vst_name(vdi_handle, 4, (char *)"FreeMono", font_name); // font_name is populated with the actual selected font 
	printf("Font %s selected\n", font_name);

	vst_map_mode(vdi_handle, 2); // Unicode mode

	return 0;
}

void gem_shutdown()
{
  v_clsvwk(vdi_handle);

  free(vdi_param_blk.control);
  free((void *)vdi_param_blk.intin);
  free(vdi_param_blk.intout);
  free((void *)vdi_param_blk.ptsin);
  free(vdi_param_blk.ptsout);

  mt_appl_exit(aes_global);
}

void window_redraw(int16_t handle, GRECT *dirty)
{
	GRECT box;

	graf_mouse(M_OFF, 0x0L);
	wind_update(BEG_UPDATE);
	wind_get(handle, WF_FIRSTXYWH, &box.g_x, &box.g_y, &box.g_w, &box.g_h);

	/* Redraw the dirty rectangle. */
	while(box.g_w && box.g_h)
	{
		if(rc_intersect(&screen_full, &box))
	{
		if(rc_intersect(dirty, &box))
			if(handle == accountWindow.GetHandle())
			{
				/* Redraw the game window. */
				set_clip(true, &box);
				accountWindow.Redraw(dirty);
			}
	}
		
		wind_get(handle, WF_NEXTXYWH, &box.g_x, &box.g_y, &box.g_w, &box.g_h);
	}

	wind_update(END_UPDATE);
	graf_mouse(M_ON, 0x0L);
}

void application_go()
{
	int16_t ev_mmox, ev_mmoy,
		ev_mmobutton, ev_mmokstate, 
		ev_mkreturn, ev_mbreturn; 	/* key / mouse button that triggered event */
  short quit = false;
  short ret, msg[8];
  GRECT dirty;

  while(!quit)
	{
		ret = mt_evnt_multi(MU_MESAG|MU_KEYBD|WM_REDRAW|WM_CLOSED|WM_MOVED, /* ev_mflags */
							2,		/* ev_mbclicks */
							1,		/* ev_mbmask */
							1,		/* ev_mm1state */
							0,		/* ev_mm1flags */
							0, 0,	        /* ev_mm1x and ev_mm1y */
							0, 0,	        /* ev_mm1width and ev_mm1height */
							0,		/* ev_mm2flags */
							0, 0,	        /* ev_mm2x and ev_mm2y */
							0, 0,	        /* ev_mm2width and ev_mm2height */
							msg,	        /* ev_mmgpbuff */
							0,	        /* ev_mt(lo|hi)count */
							&ev_mmox, &ev_mmoy,
							&ev_mmobutton, &ev_mmokstate,
							&ev_mkreturn, &ev_mbreturn,
							aes_global);

		if(ret & MU_MESAG) /* We got a message from AES. */
		{
			switch(msg[0])
			{
				case WM_REDRAW:
					dirty.g_x = msg[4];
					dirty.g_y = msg[5];
					dirty.g_w = msg[6];
					dirty.g_h = msg[7];
					window_redraw(msg[3], &dirty);
					break;
				case WM_CLOSED:
					quit = true;
					break;
				case WM_MOVED:
					mt_wind_set(msg[3], WF_CURRXYWH, msg[4], msg[5], msg[6], msg[7], aes_global);
					break;
			}
		}
    
		else if(ret & MU_KEYBD)
		{
			char event_char[2];
			event_char[0] = ev_mkreturn & 0xDF; //ignore caps
			event_char[1] = 0;
			
			if(event_char[0] >= 0x41 && event_char[0] <= 0x5A)
			{
				
			}
			else
			{
				//A non-alphanumeric key.
				switch(ev_mkreturn)
				{
				case CNTRL_Q:
					quit = true;
					break;
				default:
					break;
				}
			}
		}
    }
}

bool nvdi_is_loaded()
{
	return (vq_vgdos() == 0x5F46534D);
}

int main()
{
	jsmn_parser parser;
	jsmntok_t tokens[64];
	jsmn_init(&parser);

	// Make sure that we have a GDOS capable of loading fonts.
	if(!nvdi_is_loaded())
	{
		printf("NVDI not present, abort\n");
		exit(1);
	}

	gem_init();

	accountWindow.Show();
	application_go();

  gem_shutdown();

	return 0;
}