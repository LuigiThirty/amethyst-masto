#include "vdi_helpers.h"

void set_clip(int clip_flag, GRECT *area)
{
  short pxy[4];
  
  grect_to_array(area, pxy);
  vs_clip(vdi_handle, clip_flag, pxy);
}