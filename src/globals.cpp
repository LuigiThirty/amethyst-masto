#include "globals.h"

short hwchar, hhchar, hwbox, hhbox;
short appl_id, aes_handle, vdi_handle;
short screen_width, screen_height;
short aes_global[AES_GLOBMAX];
short desktop_usable_x, desktop_usable_y, desktop_usable_w, desktop_usable_h;
short nil;
short pixelxy[4];

GRECT screen_full;
VDIPB vdi_param_blk;

char str_temp_buf[1000];
size_t str_temp_len = 100;