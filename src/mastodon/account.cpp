#include "mastodon/account.h"

MastodonAccount::MastodonAccount(char *json)
{
    char buffer[200];

    jsmn_parser parser;
    jsmn_init(&parser);
    int result = jsmn_parse(&parser, json, strlen(json), tokens, 64);
    printf("Parser found %d tokens\n", result);

    // Read out the tokens.
    for (int i = 1; i < result; i++) {
        if (jsoneq(json, &tokens[i], "id") == 0) {
            id = strtol(json + tokens[i+1].start, NULL, 10);
            i++;
        }
        else if(jsoneq(json, &tokens[i], "username") == 0)
        {
            sprintf(username, "%.*s", tokens[i+1].end-tokens[i+1].start, json + tokens[i+1].start);
            i++;
        }
        else if(jsoneq(json, &tokens[i], "display_name") == 0)
        {
            sprintf(display_name, "%.*s", tokens[i+1].end-tokens[i+1].start, json + tokens[i+1].start);
            i++;
        }
        else if(jsoneq(json, &tokens[i], "followers_count") == 0)
        {
            followers_count = strtol(json + tokens[i+1].start, NULL, 10);
            i++;
        }
        else if(jsoneq(json, &tokens[i], "following_count") == 0)
        {
            following_count = strtol(json + tokens[i+1].start, NULL, 10);
            i++;
        }
        else if(jsoneq(json, &tokens[i], "statuses_count") == 0)
        {
            statuses_count = strtol(json + tokens[i+1].start, NULL, 10);
            i++;
        }
        else if(jsoneq(json, &tokens[i], "note") == 0)
        {
            memcpy(note, json + tokens[i+1].start, tokens[i+1].end-tokens[i+1].start);
            i++;
        }
    }

    printf("id: %d\n", id);

}